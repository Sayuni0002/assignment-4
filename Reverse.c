#include <stdio.h>
int main() {
    int m, x = 0, s;
    printf("Enter an integer: ");
    scanf("%d", &m);
    while (m != 0)
        {
        s = m % 10;
        x = x * 10 + s;
        m = m/10;
       }
    printf("Reversed number = %d", x);
    return 0;
}
